import React from 'react'

/**
 * Regular expressions for detecting and validating provider URLs
 * @type {{htmlReplacement: RegExp, main: RegExp, sanitize: RegExp}}
 */

const regex = {
  sanitize: /(https?:)?\/\/rumble.com\/embed\/([-A-Za-z0-9]+)\/?(\?[=&\-a-zA-Z0-9]+)?/i,
  main: /https?:\/\/rumble.com\/embed\/([-a-zA-Z0-9]+)\/?(\?[=&\-a-zA-Z0-9]+)?/i,
  contentID: /\/embed\/([-a-zA-Z0-9]+)/,
}
export default regex

/**
 * Configuration for HTML iframe's `sandbox` attribute
 * @type {useSandbox: boolean, sandboxAttributes: string[]}
 */
export const sandboxConfig = {
  useSandbox: false,
  sandboxAttributes: []
}

/**
 * Check if the iframe code in the post editor is to an allowed URL
 * <iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v6itwla/?pub=4" frameborder="0" allowfullscreen></iframe>
 *
 * @param url
 * @returns {boolean|*}
 */
export function validateIframeUrl (url) {
  const match = url.match(regex.sanitize)

  if (match && match.length >= 2) {
    return `https://rumble.com/embed/${match[2]}/?pub=4`
  }

  return false
}

/**
 * Rewrites the embedded URL to a normalized format
 * @param url
 * @returns {string|boolean}
 */
export function normalizeEmbedUrl (url) {
  const match = url.match(regex.contentID)

  if (match && match.length >= 2) {
      return `https://rumble.com/embed/${match[1]}/?pub=4`
    }

    return false
}

/**
 * Extract the content ID and other metadata from the URL
 * @param data
 * @returns {null|{id: *, canonical: string, url: *}}
 */
function extractMetadata (data) {
  if (!data) return null

  const m = data.match(regex.main)

  if (!m || m.length < 2) return null

  return {
    id: m[1],
    url: m[0],
    canonical: `https://rumble.com/embed/${m[1]}/?pub=4`
  }
}

export function embedNode (child, links /* images */) {
  try {
    const { data } = child
    const rumble = extractMetadata(data)
    if (!rumble) return child

    child.data = data.replace(
      rumble.url,
            `~~~ embed:${rumble.id} rumble ~~~`
    )

    if (links) links.add(rumble.canonical)
  } catch (error) {
    console.error(error)
  }

  return child
}

/**
 * Generates the Markdown/HTML code to override the detected URL with an iFrame
 * @param idx
 * @param id
 * @param width
 * @param height
 * @returns {*}
 */
export function genIframeMd (idx, id, width, height) {
  const url = `https://rumble.com/embed/${id}/?pub=4`

  let sandbox = sandboxConfig.useSandbox
  if (sandbox) {
    if (
      Object.prototype.hasOwnProperty.call(
        sandboxConfig,
        'sandboxAttributes'
      )
    ) {
      sandbox = sandboxConfig.sandboxAttributes.join(' ')
    }
  }
  const aspectRatioPercent = (height / width) * 100
  const iframeProps = {
    src: url,
    width,
    height,
    frameBorder: '0',
    allowFullScreen: ''
  }
  if (sandbox) {
    iframeProps.sandbox = sandbox
  }

  return (
    <div
      key={`rumble-${id}-${idx}`}
      className='videoWrapper'
      style={{
        position: 'relative',
        width: '100%',
        height: 0,
        paddingBottom: `${aspectRatioPercent}%`
      }}
    >
      <iframe
        title='rumble.com embedded player'
                // eslint-disable-next-line react/jsx-props-no-spreading
        {...iframeProps}
      />
    </div>
  )
}
