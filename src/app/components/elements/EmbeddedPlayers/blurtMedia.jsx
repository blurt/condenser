import React from 'react'

/**
 * Regular expressions for detecting and validating provider URLs
 * @type {{htmlReplacement: RegExp, main: RegExp, sanitize: RegExp}}
 */

const regex = {
  sanitize: /(https?:)?\/\/blurt.media\/videos\/embed\/([-A-Za-z0-9]+)(\?[=&-a-zA-Z0-9]+)?/i,
  main: /https?:\/\/blurt\.media\/(?:w\/|videos\/embed\/)([-a-zA-Z0-9]+)(\?[=&-a-zA-Z0-9]+)?[^ ]*/i,
  contentID: /(?:w\/|videos\/embed\/)([-a-zA-Z0-9]+)/,
}
export default regex

/**
 * Configuration for HTML iframe's `sandbox` attribute
 * @type {useSandbox: boolean, sandboxAttributes: string[]}
 */
export const sandboxConfig = {
  useSandbox: true,
  sandboxAttributes: ['allow-same-origin', 'allow-scripts', 'allow-popups', 'allow-forms']
}

/**
 * Check if the iframe code in the post editor is to an allowed URL
 * <iframe title="My Watercolor painting Timelapse - Winter Cabin" width="560" height="315" src="https://blurt.media/videos/embed/8f099bb2-36cc-4acf-8f15-c3c5b6d291f5" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
 *
 * @param url
 * @returns {boolean|*}
 */
export function validateIframeUrl (url) {
  const match = url.match(regex.sanitize)

  if (match && match.length >= 2) {
    return `https://blurt.media/videos/embed/${match[2]}`
  }

  return false
}

/**
 * Rewrites the embedded URL to a normalized format
 * @param url
 * @returns {string|boolean}
 */
export function normalizeEmbedUrl (url) {
  const match = url.match(regex.contentID)

  if (match && match.length >= 2) {
      return `https://blurt.media/videos/embed/${match[1]}`
    }

    return false
}

/**
 * Extract the content ID and other metadata from the URL
 * @param data
 * @returns {null|{id: *, canonical: string, url: *}}
 */
function extractMetadata (data) {
  if (!data) return null

  const m = data.match(regex.main)

  if (!m || m.length < 2) return null

  return {
    id: m[1],
    url: m[0],
    canonical: `https://blurt.media/w/${m[1]}`
  }
}

export function embedNode (child, links /* images */) {
  try {
    const { data } = child
    const blurtMedia = extractMetadata(data)
    if (!blurtMedia) return child

    child.data = data.replace(
      blurtMedia.url,
            `~~~ embed:${blurtMedia.id} blurtMedia ~~~`
    )

    if (links) links.add(blurtMedia.canonical)
  } catch (error) {
    console.error(error)
  }

  return child
}

/**
 * Generates the Markdown/HTML code to override the detected URL with an iFrame
 * @param idx
 * @param id
 * @param width
 * @param height
 * @returns {*}
 */
export function genIframeMd (idx, id, width, height) {
  const url = `https://blurt.media/videos/embed/${id}`

  let sandbox = sandboxConfig.useSandbox
  if (sandbox) {
    if (
      Object.prototype.hasOwnProperty.call(
        sandboxConfig,
        'sandboxAttributes'
      )
    ) {
      sandbox = sandboxConfig.sandboxAttributes.join(' ')
    }
  }
  const aspectRatioPercent = (height / width) * 100
  const iframeProps = {
    src: url,
    width,
    height,
    frameBorder: '0',
    allowFullScreen: ''
  }
  if (sandbox) {
    iframeProps.sandbox = sandbox
  }

  return (
    <div
      key={`blurt-media-${id}-${idx}`}
      className='videoWrapper'
      style={{
        position: 'relative',
        width: '100%',
        height: 0,
        paddingBottom: `${aspectRatioPercent}%`
      }}
    >
      <iframe
        title='blurt.media embedded player'
                // eslint-disable-next-line react/jsx-props-no-spreading
        {...iframeProps}
      />
    </div>
  )
}
