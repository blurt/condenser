import React from 'react';

function BlurtLogo() {
    return (
        <span className="logo">
            <img
                alt="Logo"
                src="/images/blurt-logo-blogger.gif"
                width="150"
                height="40"
            />
        </span>
    );
}

export default BlurtLogo;
